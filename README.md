## How to run

In the project directory, you can run:
### `npm install`
### `npm start`


## Short Explanation

The logic of the map generation is located inside utils/mapGenerate.ts<br>
There there are some functions :

<b>generateEmptyMap:</b> Initializes a two dimensional array of strings with '#'<br>
<b>findDimensions:</b> When a room size is selected , it returns two random factors as width and height of the room. (widthFactor*heightFactor=roomSize)<br>
<b>checkForDot:</b> Scans a room for character '.' and is used to filter out visually badly generated rooms.<br>
<b>generateRoom:</b> Generates a room by filling a two dimensional array of strings. Key point is that after room generation it returns the room surrounded by '#'. '#' are considered in a way as part of the room. Room size translated in how many '.' characters it contains. When generating a room we add 'X' on the number position as a flag for future use.<br>
<b>generateMap:</b> Generates the final map. It contains two helper functions that check if a room collapses with another and if a room is within the bounds of the map.Then it tries fitting as many rooms as it can inside the map taking into consideration the maxRooms variable.
If for some reason because of our variables , we can't add any more rooms to the map , we don't want to fall in an endless loop that's why we added an attempt counter to help us with that.
<br>
<b>numberizeRooms:</b> Numberin goes from top to bottom and left to right. Exactly just like our for loop. When we find 'X' we instead place the next available number as label for the room.


## Anything extra you would have done given more time

You will probably be wondering why I used react for a simple assignment that would use a console to print out the results.
I initially just wanted to avoid typeless javascript and vanilla DOM manipulation and wanted to make something more "testable" with bare hands.
My first plan was to make something more fancy , maybe something like a form , that initially you add only map width and map height , you get a map display , then you get the display of some randomized rooms . I also would like to add some cool transitions and animations , but then I realized that , that's not the point of the assigment and that considering that i have to travel for the weekend , I dont have enough time. Therefore , there will be some components inside the components directory that were initially created for other usages but ended up getting used for debugging.

