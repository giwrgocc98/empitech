import React from 'react';
import {generateEmptyMap} from "../utils/generateMap";

interface MapPreviewProps {
    mapWidth:number;
    mapHeight:number;
}
export const MapPreview: React.FC<MapPreviewProps> = ({mapWidth,mapHeight}) => {
    const mapData = generateEmptyMap(mapWidth,mapHeight);
    return (
        <div style={{display:"flex",flexDirection:"column",textAlign:"center",width: '100%'}}>
            <h1>Map Size Preview</h1>
            <div style={{width: '100%',textAlign:"center"}}>
                <pre>
                    {mapData.map((row:string[], rowIndex:number) => (
                        <div key={rowIndex}>{row.join('')}</div>
                    ))}
                </pre>
            </div>
        </div>
    )
};