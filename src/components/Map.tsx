import React from 'react';

interface MapProps {
    mapData:string[][];
}
export const Map: React.FC<MapProps> = ({mapData}) => {
    return (
        <div style={{width: '100%',textAlign:"center"}}>
            <pre>
                {mapData.map((row:string[], rowIndex:number) => (
                    <div key={rowIndex}>{row.join('')}</div>
                ))}
            </pre>
        </div>
    )
};