import React, {useState} from 'react';
import Slider from "./Slider";


interface MapSettingsProps {
    mapWidth:number;
    setMapWidth:(value:number)=>void;
    mapHeight:number;
    setMapHeight:(value:number)=>void;
    minRoomSize:number;
    setMinRoomSize:(value:number)=>void;
    maxRoomSize:number;
    setMaxRoomSize:(value:number)=>void;
    maxRooms:number;
    setMaxRooms:(value:number)=>void;
}
export const MapSettings: React.FC<MapSettingsProps> = ({mapWidth, setMapWidth , mapHeight , setMapHeight,minRoomSize,setMaxRoomSize,maxRoomSize,setMinRoomSize , maxRooms,setMaxRooms}) => {
    return (
        <div style={{width: '30%', textAlign: "center"}}>
            <Slider label={"Map Width"} min={1} max={100} value={mapWidth} setValue={setMapWidth}/>
            <Slider label={"Map Height"} min={1} max={100} value={mapHeight} setValue={setMapHeight}/>
            <Slider label={"Min Room Size"} min={1} max={100} value={minRoomSize} setValue={setMinRoomSize}/>
            <Slider label={"Max Room Size"} min={1} max={200} value={maxRoomSize} setValue={setMaxRoomSize}/>
            <Slider label={"Max Rooms"} min={5} max={20} value={maxRooms} setValue={setMaxRooms}/>
        </div>
    );
};

