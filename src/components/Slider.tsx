import React, { useState } from 'react';

type SliderProps = {
    label: string;
    min: number;
    max: number;
    value: number;
    setValue: (value: number) => void;
};

const Slider: React.FC<SliderProps> = ({label, min, max , value,setValue}) => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(parseInt(event.target.value));
    };
    return (
        <div>
            <input
                type="range"
                min={min}
                max={max}
                value={value}
                onChange={handleChange}
            />
            <p>{label} = {value}</p>
        </div>
    );
};

export default Slider;