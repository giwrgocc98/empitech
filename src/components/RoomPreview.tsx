import React from 'react';
import {generateRoom} from "../utils/generateMap";

interface MapPreviewProps {
    minRoomSize:number;
    maxRoomSize:number;
}
export const RoomPreview: React.FC<MapPreviewProps> = ({minRoomSize,maxRoomSize}) => {
    const mapData = generateRoom(minRoomSize,maxRoomSize);
    console.log(mapData)
    return (
        <div style={{display:"flex",flexDirection:"column",textAlign:"center",width: '100%'}}>
            <h1>Random Room Preview</h1>
            <div style={{width: '100%',textAlign:"center"}}>
                <pre>
                    {mapData?.map((row:string[], rowIndex:number) => (
                        <div key={rowIndex}>{row.join('')}</div>
                    ))}
                </pre>
            </div>
        </div>
    )
};