export const generateEmptyMap = (mapWidth: number, mapHeight: number): string[][] => {
    const mapArray: string[][] = [];
    for (let row = 0; row < mapHeight; row++) {
        const rowArray: string[] = [];
        for (let col = 0; col < mapWidth; col++) {
            rowArray.push('#');
        }
        mapArray.push(rowArray);
    }

    return mapArray;
}

export const generateMap = (mapWidth: number, mapHeight: number, minRoomSize: number, maxRoomSize: number, maxRooms: number): string[][] => {
    const mapArray = generateEmptyMap(mapWidth, mapHeight);
    const rooms: { x: number; y: number; width: number; height: number }[] = [];

    const doRoomsOverlap = (room1: { x: number; y: number; width: number; height: number }, room2: { x: number; y: number; width: number; height: number }): boolean => {
        return room1.x < room2.x + room2.width &&
            room1.x + room1.width > room2.x &&
            room1.y < room2.y + room2.height &&
            room1.y + room1.height > room2.y;
    };

    const isRoomWithinBounds = (room: { x: number; y: number; width: number; height: number }): boolean => {
        return room.x >= 1 && room.x + room.width + 1 <= mapWidth && room.y >= 1 && room.y + room.height + 1 <= mapHeight;
    };

    for (let z = 0; z < maxRooms; z++) {
        const roomArray = generateRoom(minRoomSize, maxRoomSize);
        if (!roomArray) {
            console.error("Failed to generate room array.");
            continue;
        }
        let roomFits = false;
        let attempts = 0;

        while (!roomFits && attempts < 100) {
            const randomStartingPosition: { x: number, y: number } = {
                x: Math.floor(Math.random() * (mapWidth - roomArray[0].length - 3)) + 1,
                y: Math.floor(Math.random() * (mapHeight - roomArray.length - 3)) + 1
            };
            const room: { x: number; y: number; width: number; height: number } = {
                x: randomStartingPosition.x,
                y: randomStartingPosition.y,
                width: roomArray[0].length,
                height: roomArray.length
            };

            if (isRoomWithinBounds(room) && !rooms.some(existingRoom => doRoomsOverlap(existingRoom, room))) {
                roomFits = true;
                rooms.push(room);
                for (let i = 0; i < roomArray.length; i++) {
                    for (let j = 0; j < roomArray[i].length; j++) {
                        mapArray[randomStartingPosition.y + i][randomStartingPosition.x + j] = roomArray[i][j];
                    }
                }
            }

            attempts++;
        }
    }

    return numberizeRooms(mapArray);
}

const numberizeRooms = (map : string[][]) => {
    let counter = 1;
    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[i].length; j++) {
            if(map[i][j]==='X'){
                map[i][j] = counter.toString();
                counter++;
            }

        }
    }
    return map;
}

export const generateRoom = (minRoomSize: number, maxRoomSize: number): string[][] | null => {
    const roomSize = Math.floor(Math.random() * (maxRoomSize - minRoomSize + 1)) + minRoomSize;
    const { width: roomWidth, height: roomHeight } = findDimensions(roomSize);
    if (!roomWidth || !roomHeight) {
        console.error("Failed to calculate room dimensions.");
        return null;
    }
    const roomArray: string[][] = [];

    const emptyTopArray: string[] = [];
    const topRowArray: string[] = [];
    topRowArray.push("#")
    for (let col = 0; col < roomWidth; col++) {
        topRowArray.push('_');
    }
    topRowArray.push("#")
    for (let col = 0; col < roomWidth+2; col++) {
        emptyTopArray.push('#')
    }
    roomArray.push(emptyTopArray);
    roomArray.push(topRowArray);

    for (let row = 0; row < roomHeight; row++) {
        const rowArray: string[] = [];
        rowArray.push('#');
        rowArray.push('|');
        for (let col = 0; col < roomWidth - 2; col++) {
            if(row===0 && col===0){
                rowArray.push('X')
            }
            else {
                rowArray.push('.');
            }
        }
        rowArray.push('|');
        rowArray.push('#');
        roomArray.push(rowArray);
    }

    const bottomRowEmptyArray: string[] = [];
    const bottomRowArray: string[] = [];
    bottomRowArray.push("#")
    for (let col = 0; col < roomWidth; col++) {
        bottomRowArray.push('_');
    }
    bottomRowArray.push("#")
    for (let col = 0; col < roomWidth+2; col++) {
        bottomRowEmptyArray.push('#')
    }
    roomArray.push(bottomRowArray);
    roomArray.push(bottomRowEmptyArray);

    return checkForDot(roomArray);
}

function findDimensions(roomSize: number) {
    let results: { width: number, height: number }[] = [{ width: 1, height: roomSize }];

    for (let i = 2; i <= Math.sqrt(roomSize); i++) {
        if (roomSize % i === 0) {
            results.push({ width: i, height: roomSize / i });
        }
    }

    const randomResult = results[Math.floor(Math.random() * results.length)];
    if(randomResult.height === 1 || randomResult.width === 1){
        return {width:undefined , height:undefined};
    }
    if (Math.random() < 0.5) {
        return { width: randomResult.height, height: randomResult.width };
    } else {
        return randomResult;
    }
}

function checkForDot(array:string[][]) {
    for (let row of array) {
        if (row.includes('.')) {
            return array;
        }
    }
    return null;
}