import React, {useState} from 'react';
import './App.css';
import {Map} from "./components/Map";
import {MapSettings} from "./components/MapSettings";
import {generateMap} from "./utils/generateMap";
import {MapPreview} from "./components/MapPreview";
import {RoomPreview} from "./components/RoomPreview";

function App() {
    const [mapWidth,setMapWidth] = useState<number>(70)
    const [mapHeight,setMapHeight] = useState<number>(30)
    const [minRoomSize,setMinRoomSize] = useState<number>(10)
    const [maxRoomSize,setMaxRoomSize] = useState<number>(20)
    const [maxRooms,setMaxRooms] = useState<number>(5)
    const mapData = generateMap(mapWidth,mapHeight,minRoomSize,maxRoomSize,maxRooms);

      return (
        <div className="App">
            <MapSettings mapWidth={mapWidth}
                         setMapWidth={setMapWidth}
                         mapHeight={mapHeight}
                         setMapHeight={setMapHeight}
                         minRoomSize={minRoomSize}
                         setMinRoomSize={setMinRoomSize}
                         maxRoomSize={maxRoomSize}
                         setMaxRoomSize={setMaxRoomSize}
                         maxRooms={maxRooms}
                         setMaxRooms={setMaxRooms}
            />
            {/*<MapPreview mapWidth={mapWidth} mapHeight={mapHeight}/>*/}
            {/*<RoomPreview minRoomSize={minRoomSize} maxRoomSize={maxRoomSize}/>*/}
            <Map mapData={mapData}/>
        </div>
      );
}

export default App;
